
// We learned how to gather data from the user input using a prompt();
    function printInfo(){
        let nickName = prompt("Enter your nickname: ");
        console.log("Hi, " + nickName + "!");
    }
    // printInfo();

    function printName(name){
        console.log("My name is " + name);
    }
    
    printName("Juana");
    printName("John");

// [SECTION] Parameters and Arguments
// Parameter
    // "name" is called a parameter
    // A "parameter" acts as a named variable/container that exists only inside the function
    // It is used to store information that is provided to a function when it is called/invoked

// Argument
    // "Juana" and "John", the information/data provided directly into the function is called argument
    // Values passed when invoking a function are called arguments
    // These arguments are then stored within the function

// Variables can also be passed as an argument
    let sampleVariable = "Yui";
    printName(sampleVariable);
    printName(); // function calling without arguments will result to undefined parameters

// Check if a specific number is divisible by 8
    function checkDivisibilityBy8(num){
        let remainder = num % 8;
        console.log("The remainder of " + num + " divided by 8 is " + remainder);
        let isDivisibleBy8 = remainder === 0;
        console.log("Is " + num + " divsible by 8?");
        console.log(isDivisibleBy8);
    }
    checkDivisibilityBy8(64);
    checkDivisibilityBy8(28);

// [SECTION] Function as argument
    // Function parameters can also accept other functions as arguments
    // Some complex functions uses other functions as arguments to perform more complicated result

    function argumentFunction(){
        console.log("This function was passed as an argument before the message was printed");
    }
    
    function invokeFunction(argumentFunction){
        argumentFunction();
    }
    // A function used without a parenthesis is normally associated with using the function as an argument
    invokeFunction(argumentFunction);
    console.log(argumentFunction)

// [SECTION] Using Multiple Parameters
    // Multiple "arguments" will correspond to the number of "parameters" declared in a function in a succeeding order

    function createFullName(firstName, middleName, lastName){
        console.log("My full name is " + firstName + " " + middleName + " " + lastName);
    }
    createFullName("TJ", "L", "Clemente");
    // In javascript providing more/less arguments than expected parameters will not return an error
    createFullName("TJ", "Clemente");
    createFullName("TJ", "L", "Clemente", "Hello");

    // Using variables as argument
    let firstName = "John";
    let middleName = "Doe";
    let lastName = "Smith";
    createFullName(firstName, middleName, lastName);

// [SECTION] The return statement
    // The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called

    function returnFullName(firstName, middleName, lastName){
        return firstName + " " + middleName + " " + lastName;
        // console.log("This is printed inside the function"); //Unreachable code detected
    }
    
    // Value returned from the function is stored in our variable
    let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
    console.log(completeName);
    // Using return statement value, we can further use/manipulate in our program instead of only printing/displaying in our console log
    console.log("My name is: " + completeName);
    console.log(returnFullName("Jeffrey", "Smith", "Bezos"));
    
